﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations.Schema;

namespace NoComprendoMyFrendo.Models
{
    public class RecordDto
    {
        public RecordDto(int id,int puntos , int teamId, string description)
        {
            Id = id;
            Puntos = puntos;    
            TeamId = teamId;
            Description = description;
        }
        public int Id { get; set; }
        public string Description { get; set; }
        public int Puntos { get; set; }
        public int TeamId { get; set; }


    }
}
