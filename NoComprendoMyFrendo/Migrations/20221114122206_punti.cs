﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NoComprendoMyFrendo.Migrations
{
    public partial class punti : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Puntos",
                table: "Record",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Puntos",
                table: "Record");
        }
    }
}
