﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NoComprendoMyFrendo.Models
{
    public class Team
    {
        public Team()
        {
            Record = new List<Record>();
        }


        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [InverseProperty("Team")]
        List<Record> Record { get; set; }
    }
}
