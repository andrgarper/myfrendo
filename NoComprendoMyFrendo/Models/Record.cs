﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NoComprendoMyFrendo.Models
{
    public class Record
    {

        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        public int Puntos { get; set; }
        public int TeamId { get; set; }
        [ForeignKey("TeamId")]
        [ValidateNever]
        public Team Team { get; set; }

    }
}
